---
layout: post
Title: Protein Bioinformatics Exercise Report
---

# Exercise 2 

Nomura Akira, Kleinschmidt Noah

---

For this exercise we implemented a command-line tool to compute the positional variance of protein residues among related structures.

### Requirements

The script requires 

Default anaconda pre-installed:
- `numpy`
- `pandas`
- `matplotlib`
- `seaborn`

Additional requirements:
- `biopython`
- `biopandas`
- `PyMol`
- `PyQt5` (on Mac, for `PyMol`)
- `argparse` (for the command-line interface)
- `halo` (for the funny spinners)

> There is a dedicated `protbio.yaml` environments specification file which will set up a suitable environment.

### Input data

For the input, the script requires a valid `PDB` file. 

### Implementation

The algorithm obtains related proteins through a BLAST search. BLAST searching can be either done locally with a dedicated database, or remotely (default through `swissprot`). The algorithm then obtains reference structures from `PDBe` or `Alphafold` (whichever is available). It subsequently performs pairwise sequence alignment between the query and each reference to obtain identical residues which can be used for partial structure superimposition. The query structure is transformed according to the imposed model and the atom distances between query structure and and the reference post-transformation are computed. Eventually, the atom distance variance in each residue across all reference structures is computed and returned as final output. 

<img src="resources/overview.png">

### Output

The script outputs a table file assigning the mean atom variance per residue in the query sequence. It further generates a histogram of mean atom variances per residue.

<img width=60% src="results/AF-Q4V9C7-F1-model_v4.var.hist.png">

Additionally, it outputs a number of images wherein the variance in each residue is colored onto the 3D structure. Also, it downloads reference PDB files, which may or may not be regarded as further output. 

<img src="results/Q9FJR0.var.png">


### Usage

The code is available [via GitLab](https://gitlab.com/NoahHenrikKleinschmidt/protein_bioinformatics), located in the `exercises/exercise2/code` directory.

    >>> git clone https://gitlab.com/NoahHenrikKleinschmidt/protein_bioinformatics

To use the script call (from within `exercises/exercise2/code`)

    >>> python3 main.py <pdb-file> <output-directory>

The full CLI reads as follows:

```
usage: main.py [-h] [-db BLAST_DATABASE] [-r] [-p PDB_DIR] [-m MODEL] [-c CHAIN] [-f] pdb outdir

This script computes the per-residue structural positional variance of a PDB structure using
related references.

positional arguments:
  pdb                   PDB file containing the query protein structure
  outdir                Output-directory for the TSV residue-variance table and the molecular
                        visualization.

optional arguments:
  -h, --help            show this help message and exit
  -db BLAST_DATABASE, --blast-database BLAST_DATABASE
                        The database to use for reference finding. This can either be a valid
                        standard reference blast database such as 'swissprot' (default), or the
                        filepath to a locally generated protein BLAST database.
  -r, --render          Render the query structure where the per-residue variance is added as a
                        colormap.
  -p PDB_DIR, --pdb-dir PDB_DIR
                        Specify a directory in which to download PDB files.
  -m MODEL, --model MODEL
                        The model of interest in the query protein. Note: This must be available in
                        ALL PDB files!
  -c CHAIN, --chain CHAIN
                        The chain of interest in the query protein. Note: This must be available in
                        ALL PDB files!
  -f, --force           Force a re-query to the database. By default if a query output XML is
                        already present, the results will be directly loaded from there.
```
