"""
This script computes the per-residue structural positional variance
of a PDB structure using related references.
"""

import os
import argparse

import defaults
import seq_io
import blast_io
import pdb_io
import analysis
import visualise as viz

logger = defaults.logger


def cli():
    """Command line interface."""
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("pdb", help="PDB file containing the query protein structure")
    parser.add_argument("outdir", help="Output-directory for the TSV residue-variance table and the molecular visualization.")
    parser.add_argument(
        "-db",
        "--blast-database",
        help="The database to use for reference finding. This can either be a valid standard reference blast database such as 'swissprot' (default), or the filepath to a locally generated protein BLAST database.",
        default=defaults.blast_db,
    )
    parser.add_argument("-r", "--render", action="store_true", help="Render the query structure where the per-residue variance is added as a colormap.")
    parser.add_argument("-p", "--pdb-dir", default=None, help="Specify a directory in which to download PDB files.")
    parser.add_argument("-m", "--model", type=int, default=0, help="The model of interest in the query protein. Note: This must be available in ALL PDB files!")
    parser.add_argument("-c", "--chain", default="A", help="The chain of interest in the query protein. Note: This must be available in ALL PDB files!")
    parser.add_argument("-f", "--force", action="store_true", help="Force a re-query to the database. By default if a query output XML is already present, the results will be directly loaded from there.")
    return parser.parse_args()


def main():
    """Main entry point."""
    args = cli()

    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)

    output = os.path.join(args.outdir, os.path.basename(args.pdb).replace(".pdb", ""))

    # create a BlastQuery object
    # if we already have an existing Blast output XML
    # then load the results from there instead of re-querying the database
    # unless forced to...
    xml_query_out = args.pdb + ".blast.xml"
    if os.path.exists(xml_query_out) and not args.force:
        logger.info(f"Found existing BLAST query output: {xml_query_out}")
        logger.info("Loading results from there.")
        blast_query = blast_io.BlastQuery.from_xml(xml_query_out)
    else:
        blast_query = blast_io.BlastQuery.from_pdb(args.pdb, db=args.blast_database)
        blast_query.get_ref_sequences()
        blast_query.write(xml_query_out)

    # and make a structure query from the identified references
    structure_queries = pdb_io.StructureQueries.from_ids(blast_query.ref_ids)
    structure_queries.filter()  # filter out any without a PDB or Alphafold ID

    # get reference structures and sequences
    ref_structures = structure_queries.to_structures(dirname=args.pdb_dir)
    ref_sequences = structure_queries.to_sequences(full=False)

    # read the query PDB file to get query structure and sequences
    query_structure = pdb_io.read_pdb(args.pdb)
    query_sequence = seq_io.seq_from_biopandas(pdb_io.pdb_to_df(args.pdb))

    # compute the per-residue variance by super-imposing
    # the query structure onto the reference structures
    variances = analysis.superimpose(query_structure, query_sequence, ref_structures, ref_sequences, model=args.model, chain=args.chain)

    # write the results to a TSV file
    analysis.write_variance_table(query_structure, output + ".var.tsv", model=args.model, chain=args.chain)

    # visualize a variance histogram
    viz.variance_hist(variances, output + ".var.hist.png")

    # visualize the query structure with the variance as a colour gradient
    if args.render:
        viz.render_structure(query_structure, output + ".var.png", model=args.model, chain=args.chain)


if __name__ == "__main__":
    # we sometimes get a connection error with BLAST when using the remote query service
    # so we retry a few times before giving up and calling the process failed...
    for i in range(defaults.retries):
        try:
            main()
            exit()
        except Exception as e:
            logger.error(f"Failed to run main() with exception: {e}")
            logger.error(f"Retrying ({i + 1}/{defaults.retries})...")
    logger.critical("Failed to run main() after retries.")
