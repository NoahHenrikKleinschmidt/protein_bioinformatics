"""
This module handles getting reference structures from PDB or Alphafold
and manipulating PDB structures.
"""

import defaults
import seq_io

import os
from collections import namedtuple
from urllib import request

import Bio.ExPASy as expasy
import Bio.SwissProt as swissprot
import Bio.PDB as pdb

from biopandas.pdb import PandasPdb

logger = defaults.logger

__default_PDBParser__ = pdb.PDBParser()
__default_biopandas__ = PandasPdb()
__default_pdbio__ = pdb.PDBIO()

# ==============================================================================
# Structure handling
# ==============================================================================


AtomLists = namedtuple("AtomLists", ("fixed", "moving"))
"""
A moving and fixed list of atoms for superimosition
"""


def make_atom_lists(fixed, moving, residues, model=0, chain="A"):
    """
    Make lists of atoms from two structures

    Parameters
    ----------
    fixed : Bio.PDB.Structure.Structure
        The structure which will provide the "fixed" atoms list
    moving : Bio.PDB.Structure.Structure
        The structure which will provide the "moving" atoms list
    residues : tuple
        A tuple of two `numpy.ndarrays` containing the indices of identical residues
        in the two sequences. Indices are relative to unaligned sequences.
        First array is for `structure1`, second array is for `structure2`.
    model : int or tuple
        Either a single integer (in which case both structures must have a model with the given index)
        or a tuple of two integers.
    chain : str or tuple
        Either a single chain (in which case both structures must have a chain in the chosen model with that id)
        or a tuple of two strings.

    Returns
    -------
    AtomLists
        A named tuple containing lists of atoms of both structures.
    """

    atoms_fixed = []
    atoms_moving = []

    residues_fixed = residues[0]
    residues_moving = residues[1]

    if isinstance(model, (int, float)):
        model = (int(model),) * 2

    if isinstance(chain, str):
        chain = (chain,) * 2

    fixed = list(i for i in fixed[model[0]][chain[0]].get_residues() if pdb.is_aa(i))
    moving = list(i for i in moving[model[1]][chain[1]].get_residues() if pdb.is_aa(i))

    for i, j in residues_fixed:
        for s in fixed[i:j]:
            atoms_fixed += list(s.get_atoms())

    for i, j in residues_moving:
        for s in moving[i:j]:
            atoms_moving += list(s.get_atoms())

    # just as a safeguard since some PDB files can be slightly crappy
    # (if the experimental data isn't super good)
    # and we don't to rely on the same residues also having completely
    # identical atom lists necessarily...
    if not len(atoms_fixed) == len(atoms_moving):
        # logger.warning(f"Atom lists are not of the same length ({len(atoms1)} vs {len(atoms2)})! Cropping...")
        min_len = min(len(atoms_fixed), len(atoms_moving))
        atoms_fixed = atoms_fixed[:min_len]
        atoms_moving = atoms_moving[:min_len]

    return AtomLists(atoms_fixed, atoms_moving)


def read_pdb(filename):
    """
    Read a PDB file and return a Biopython structure

    Parameters
    ----------
    filename : str
        The path to the PDB file

    Returns
    -------
    Bio.PDB.Structure.Structure
        The Biopython structure
    """
    with open(filename, "r") as handle:
        return __default_PDBParser__.get_structure(filename, handle)


def write_pdb(structure, filename):
    """
    Write a Biopython structure to a PDB file

    Parameters
    ----------
    structure : Bio.PDB.Structure.Structure
        The Biopython structure
    filename : str
        The path to the PDB file
    """
    __default_pdbio__.set_structure(structure)
    __default_pdbio__.save(filename)


def get_atoms(structure, model=0, chain="A"):
    """
    Get all atoms from a specific chain.

    Parameters
    ----------
    structure : Bio.PDB.Structure.Structure
        The structure
    model : int
        The model index
    chain : str
        The chain id

    Returns
    -------
    list
        A list of atoms
    """
    return list(structure[model][chain].get_atoms())


def pdb_to_df(filename):
    """
    Convert a PDB file to a Biopandas dataframe

    Parameters
    ----------
    filename : str
        The path to the PDB file

    Returns
    -------
    biopandas.pdb.PandasPdb
        The Biopandas dataframe
    """
    return __default_biopandas__.read_pdb(filename)


# ==============================================================================
# Reference structures
# ==============================================================================

# copied from exercise1
def get_swissprot_entries(ids):
    """
    Get the UniProt entries for a list of ids

    Parameters
    ----------
    ids : list
        A list of UniProt ids

    Returns
    -------
    list
        A list of UniProt entries
    """
    entries = []

    spinner = defaults.spinner()
    spinner.start(f"Getting SwissProt entries for {len(ids)} proteins")

    for id in ids:
        logger.debug(f"entry: {id}")
        with expasy.get_sprot_raw(id) as handle:
            entry = swissprot.read(handle)
            entries.append(entry)

    spinner.succeed(f"Got SwissProt entries for {len(ids)} proteins")
    return entries

# this one became a bit bigger than I initially thought but it turned out pretty awesome.
# it is a really nice interface to get various datatypes related to structure files / databases from a single hub.
# I'm actually kinda proud of that one...
class StructureQueries:
    """
    A collection of PDB structures from PDB or Alphafold
    This collection can return a list of
    - Biopython structures,
    - Biopandas dataframes,
    - Biopython SequenceRecords,
    - PDB-filepaths.

    Parameters
    ----------
    entries : list
        A list of Bio.SwissProt.Record objects
        for which to get PDB structures
    """

    def __init__(self, entries):
        self.values = []
        self.ids = entries
        self.queries = [StructureQuery(entry) for entry in entries]

    def __repr__(self):
        return f"StructureQueries({self.ids})"

    def __iter__(self):
        return iter(self.values)

    def __len__(self):
        return len(self.values)

    def __getitem__(self, key):
        return self.values[key]

    def __setitem__(self, key, value):
        self.values[key] = value

    def __delitem__(self, key):
        del self.values[key]

    @classmethod
    def from_ids(cls, ids):
        """
        Get PDB structures from a list of UniProt ids

        Parameters
        ----------
        ids : list
            A list of UniProt ids

        Returns
        -------
        Structures
            A Structures collection object
        """
        entries = get_swissprot_entries(ids)
        return cls(entries)

    @classmethod
    def from_blast(cls, blast_query):
        """
        Get PDB structures from a blast query

        Parameters
        ----------
        blast_query : blast.BlastQuery
            A blast query object

        Returns
        -------
        Structures
            A Structures collection object
        """
        entries = get_swissprot_entries(blast_query.ref_ids)
        return cls(entries)

    def filter(self):
        """
        Filter the PDB structures to only those with a PDB or Alphafold id
        """

        filtered = []
        for entry in self.queries:
            if entry.pdb_id or entry.alphafold_id:
                filtered.append(entry)

        self.queries = filtered

    def to_structures(self, which=None, dirname=None):
        """
        Convert to Bio.PDB.Structure objects for the PDB structures.

        Parameters
        ----------
        which : str
            Which structure to get. Can be "pdb" or "alphafold".
            If None, the PDB structure is returned if it exists.
        dirname : str
            The directory to write the PDB files to.
            If None, the current directory is used.

        Returns
        -------
        list
            A list of Bio.PDB.Structure objects
        """
        self.values = []
        for entry in self.queries:
            self.values.append(entry.get_structure(which=which, dirname=dirname))
        return self.values

    def to_dfs(self, which=None):
        """
        Convert to Biopandas dataframes for the PDB structures.

        Parameters
        ----------
        which : str
            Which structure to get. Can be "pdb" or "alphafold".
            If None, the PDB structure is returned if it exists.

        Returns
        -------
        list
            A list of Biopandas dataframes
        """
        self.values = []
        for entry in self.queries:
            self.values.append(entry.get_df(which=which))
        return self.values

    def to_pdb_files(self, which=None, dirname=None):
        """
        Write the PDB structures to PDB files,
        and convert to a list of written pdb files.

        Parameters
        ----------
        which : str
            Which structure to get. Can be "pdb" or "alphafold".
            If None, the PDB structure is returned if it exists.
        dirname : str
            The directory to write the PDB files to.
            If None, the current directory is used.

        Returns
        -------
        list
            A list of PDB filepaths.
        """
        self.values = []
        for entry in self.queries:
            entry.get_pdb(which=which, dirname=dirname)
            self.values.append(entry.pdb_file)
        return self.values

    def to_sequences(self, full=True, which=None):
        """
        Convert to sequences for the PDB structures.

        Parameters
        ----------
        full : bool
            If True, the full protein sequences are read.
            Otherwise only the sequence of residues that have atoms
            associated in the structures.

        which : str
            Which structure to get. Can be "pdb" or "alphafold".
            If None, the PDB structure is returned if it exists.

        Returns
        -------
        list
            A list of sequences
        """
        self.values = []
        for entry in self.queries:
            entry.get_pdb(which=which)
            self.values.append(entry.get_sequence(full=full))
        return self.values


class StructureQuery:
    """
    A class for getting pdb structures from the RCSB PDB or AlphaFoldDB.

    Parameters
    ----------
    entry : Bio.SwissProt.Record
        The UniProt entry.

    Attributes
    ----------
    entry : Bio.SwissProt.Record
        The UniProt entry.
    accessions : list
        A list of UniProt accessions
    pdb_id : str
        The PDB id for the structure
    alphafold_id : str
        The AlphaFold id for the structure
    pdb_file : str
        The path to the PDB file that was written by `get_pdb`.
    """

    def __init__(self, entry):
        self.entry = entry
        self.accessions = entry.accessions

        self._cross_references = list(zip(*self.entry.cross_references))[0]
        self.pdb_file = None

    def __repr__(self):
        return f"StructureQuery({self.entry.accessions})"

    def __hash__(self):
        return hash(tuple(self.entry.accessions))

    def get_structure(self, which=None, filename=None, dirname=None):
        """
        Get a Bio.PDB.Structure object for the PDB structure.

        Parameters
        ----------
        which : str
            Which structure to get. Can be "pdb" or "alphafold".
            If None, the PDB structure is returned if it exists,
        filename : str
            The filename to write the PDB file to.
            If None, the PDB id is used.
        dirname : str
            The directory to write the PDB file to.
            This is an alternative to the full path provided
            as `filename`. If None, the current directory is used.
        Returns
        -------
        Bio.PDB.Structure
            A Bio.PDB.Structure object
        """
        self.get_pdb(which=which, filename=filename, dirname=dirname)
        return __default_PDBParser__.get_structure(self.accessions[0], self.pdb_file)

    def get_df(self, which=None):
        """
        Get a Biopandas dataframe for the PDB structure.

        Parameters
        ----------
        which : str
            Which structure to get. Can be "pdb" or "alphafold".
            If None, the PDB structure is returned if it exists,

        Returns
        -------
        Biopandas
            A Biopandas dataframe
        """
        self.get_pdb(which=which)
        return pdb_to_df(self.pdb_file)

    @property
    def pdb_id(self):
        """
        Get the PDB id for the UniProt entry.

        Returns
        -------
        str
            The PDB id. If no PDB id is found, None is returned.
        """
        if "PDB" in self._cross_references:
            idx = self._cross_references.index("PDB")
            return self.entry.cross_references[idx][1]

    @property
    def pdb_stats(self):
        """
        Get the PDB statistics for the UniProt entry.

        These include (in order):
        - the method used to determine the structure
        - the resolution of the structure
        - the R-factor

        Returns
        -------
        tuple
            A tuple of the PDB statistics.
            If no PDB statistics are found, None is returned.
        """
        if "PDB" in self._cross_references:
            idx = self._cross_references.index("PDB")
            return self.entry.cross_references[idx][2:]

    @property
    def alphafold_id(self):
        """
        Get the AlphaFold id for the UniProt entry.

        Returns
        -------
        str
            The AlphaFold id. If no AlphaFold id is found, None is returned.
        """
        if "AlphaFoldDB" in self._cross_references:
            idx = self._cross_references.index("AlphaFoldDB")
            return self.entry.cross_references[idx][1]

    def get_pdb(self, which=None, filename=None, dirname=None):
        """
        Download the PDB structure for the UniProt entry.

        Parameters
        ----------
        which : str, optional
            Which database to use. Either "pdb" or "alphafold".
            By default if a PDB id is found, the PDB database is used.
        filename : str, optional
            The filename to save the PDB structure to. If not given, the PDB id is used.
        dirname : str, optional
            The directory to save the PDB structure to. This is an alternative to the full
            path in `filename`.

        Returns
        -------
        str
            The filename of the downloaded PDB structure.
        """

        id, handle = self._get_pdb(which=which)

        if not dirname:
            dirname = defaults.pdb_structure_dir

        if not filename:
            filename = dirname + f"/{id}.pdb"

            if not os.path.exists(dirname):
                os.makedirs(dirname)

            elif os.path.exists(filename):
                logger.info(f"File {filename} already exists. Skipping.")
                self.pdb_file = filename
                return

        with open(filename, "w") as f:
            handle = handle.read().decode("utf-8")
            f.write(handle)

        self.pdb_file = filename

    def get_sequence(self, full=True):
        """
        Get the sequence for the UniProt entry from the PDB file.

        Note
        ----
        This requires that a pdb structure was obtained
        from PDB or Alphafold beforehand.

        Parameters
        ----------
        full : bool, optional
            Whether to return the full sequence or just the sequence
            of residues that also have atoms associated in the structure.

        Returns
        -------
        Bio.Seq.Seq
            The sequence of the structure.
        """
        if not self.pdb_file:
            raise ValueError("No PDB file found. Please get a PDB file first.")

        if full:
            seq = seq_io.read_seq_from_pdb(self.pdb_file)
        else:
            seq = seq_io.seq_from_biopandas(self.get_df())
        return seq

    def _get_pdb(self, which=None):
        """
        Get the PDB structure for the UniProt entry.

        Parameters
        ----------
        which : str, optional
            Which database to use. Either "pdb" or "alphafold".
            By default if a PDB id is found, the PDB database is used.

        Returns
        -------
        _UrlOpen
            A handle for the PDB structure file.
        """
        if which == "pdb" and not self.pdb_id:
            raise ValueError("No PDB id found for this entry.")

        elif which == "alphafold" and not self.alphafold_id:
            raise ValueError("No AlphaFold id found for this entry.")

        if which == "pdb" and self.pdb_id:
            id = self.pdb_id
            url = defaults.pdb_url.format(id=id)
            where = "PDB"

        elif which == "alphafold" and self.alphafold_id:
            id = self.alphafold_id
            url = defaults.alphafold_url.format(id=id)
            where = "Alphafold"

        elif self.pdb_id:
            id = self.pdb_id
            url = defaults.pdb_url.format(id=id)
            where = "PDB"

        elif self.alphafold_id:
            id = self.alphafold_id
            url = defaults.alphafold_url.format(id=id)
            where = "Alphafold"

        else:
            raise ValueError("No PDB or AlphaFold id found for this entry.")

        spinner = defaults.spinner()
        spinner.start(f"Downloading PDB structure {id} from {where}...")

        handle = request.urlopen(url)

        spinner.succeed(f"Downloaded PDB structure {id}")
        return id, handle
