"""
Default settings
"""

import logging
import random

# try importing some spinners so we can have a more entertaining experience
try:
    import halo
except ImportError:
    import subprocess

    subprocess.call(["pip", "install", "halo"])
    import halo


# =============================================================================
#  General
# =============================================================================

logger = logging.getLogger("prot-log")
"""
Logger for the application.
"""
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())

retries = 3
"""
Number of times to retry a failed process.
"""

use_spinner = True
"""
Enable or disable a spinner for long processes
"""

spinner_style = random.choice(("dots4", "monkey", "earth", "bouncingBall", "arrow3", "boxBounce"))
"""
The spinner style to use
"""


class DummySpinner:
    """
    Just a dummy so that we can code uniformly
    but still enable or disable spinning...
    """

    def __init__(self, *args, **kwargs):
        self.text = None
        self.spinner = None

    def start(self, text=None):
        if text:
            self.text = text
        print(f"Started {self.text}...")

    def stop(self):
        pass

    def succeed(self, text):
        print(text)

    def fail(self, text):
        print(text)


def spinner():
    """
    Spinner to use for loading.
    And keep the user a bit entertained...
    """
    if use_spinner:
        return halo.Halo(text="Loading", spinner=spinner_style)
    else:
        return DummySpinner()


# =============================================================================
#  Reference Handling
# =============================================================================

blast_db = "swissprot"
"""
Name of the BLAST database to use.
"""

blast_query_settings = {}
"""
Default settings for BLAST queries.
These must be valid arguments for either the remote `qblast` or the local `bastlp`.
"""

pdb_structure_dir = "pdb_structures"
"""
Directory to save PDB structures to.
"""

pdb_structure_settings = {}
"""
Default settings for PDB structure queries. 
This must be a valid PDB JSON query format.
"""

pdb_url = "https://files.rcsb.org/download/{id}.pdb"
"""
URL to download PDB structures from.
"""


alphafold_url = f"https://alphafold.ebi.ac.uk/files/AF-{{id}}-F1-model_v4.pdb"
"""
URL to download AlphaFold structures from.
"""

# =============================================================================
#  Rendering
# =============================================================================

cmap = "Spectral_r"
"""
The colormap to use for rendering.
"""

pymol_save_kws = dict(width=1500, height=1500, dpi=800, ray=1)
"""
Default keyword arguments to pass to pymol.png
"""
