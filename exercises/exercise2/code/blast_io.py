"""
This module handles getting reference sequences from a BLAST database.
"""

import defaults
import seq_io as seq

import os, tempfile
import subprocess

import Bio.Blast.NCBIWWW as blast_remote
import Bio.Blast.Applications as blast_local
from Bio.Blast import NCBIXML
from io import StringIO

logger = defaults.logger


class BlastQuery:
    """
    Class to handle BLAST queries.

    Parameters
    ----------
    query_sequence : str
        The query protein sequence. In Fasta format
        (one string including header and sequence)
    db : str, optional
        The BLAST database to use. By default the default database is used.
    """

    def __init__(self, query_sequence, db=None):
        self.query_sequence = query_sequence
        self.db = db

        self._blast_records = None

    def __repr__(self):
        return f"BlastQuery({self.query})"

    @classmethod
    def from_xml(cls, xml_file):
        """
        Create a BlastQuery object from an existing BLAST query XML file.

        Parameters
        ----------
        xml_file : str
            The BLAST XML file.
        Returns
        -------
        BlastQuery
            The BlastQuery object.
        """
        with open(xml_file) as f:
            query_sequence = f.read().strip()
        new = cls(None, None)
        new._blast_records = query_sequence
        return new

    @classmethod
    def from_pdb(cls, pdb_file, db=None):
        """
        Create a BlastQuery object from a PDB file.

        Parameters
        ----------
        pdb_file : str
            The PDB file to read the query sequence from.
        db : str, optional
            The BLAST database to use. By default the default database is used.

        Returns
        -------
        BlastQuery
            The BlastQuery object.
        """
        # get the sequence from the pdb file
        sequence = seq.read_seq_from_pdb(pdb_file)
        # convert the sequence to a FASTA-format string
        sequence = f">{sequence.description}\n{sequence.seq}"
        return cls(sequence, db=db)

    def get_ref_sequences(self):
        """
        Get reference sequences from the blast database.

        Returns
        -------
        str
            The BLAST results in XML format.
        """
        query_sequence = self.query_sequence

        logger.debug(f"ref-db: {defaults.blast_db}")
        logger.debug(f"query-seq: {query_sequence}")

        if not query_sequence.startswith(">"):
            raise ValueError("Query sequence must be in Fasta format.")

        spinner = defaults.spinner()
        spinner.start(f"Getting references from {defaults.blast_db} (this may take a while)")

        if self.db is None or not os.path.exists(self.db + ".phr"):
            logger.debug("Using remote BLAST")
            blastp = _blast_query_remote(query_sequence, self.db)
        else:
            logger.debug("Using local BLAST")
            blastp = _blast_query_local(query_sequence, self.db)

        spinner.succeed(f"Got references from {defaults.blast_db}")

        self._blast_records = "".join(list(blastp))
        return self._blast_records

    def write(self, filename):
        """
        Write the BLAST results to a file.

        Parameters
        ----------
        filename : str
            The filename to write the BLAST results to.
        """
        with open(filename, "w") as f:
            f.write(self._blast_records)

    @property
    def ref_ids(self):
        """
        Get the reference hit IDs from the BLAST results.

        Returns
        -------
        list
            A list of the reference hit IDs.
        """
        records = StringIO(self._blast_records)
        blast_records = NCBIXML.parse(records)

        hit_ids = []
        for record in blast_records:
            hit_ids += [hit.accession for hit in record.alignments]

        records.close()
        return hit_ids


def _blast_query_remote(query, db=None):
    """
    Perform a remote BLAST query using the web-interface of NCBI.

    Parameters
    ----------
    query : str
        The query protein sequence. In Fasta format
        (one string including header and sequence)
    db : str, optional
        The BLAST database to use. By default the default database is used.

    Returns
    -------
    stringIO
        The BLAST results in XML format.
    """
    if not db:
        db = defaults.blast_db

    blastp = blast_remote.qblast(
        "blastp",
        db,
        query,
        **defaults.blast_query_settings,
    )
    return blastp


def _blast_query_local(query, db):
    """
    Perform a local BLAST query using the BLAST+ command-line tool.

    Parameters
    ----------
    query : str
        The query protein sequence. In Fasta format
        (one string including header and sequence), or a file path to a Fasta file.
    db : str
        The BLAST database to use.

    Returns
    -------
    stringIO
        The BLAST results in XML format.
    """
    if subprocess.call(["which", "blastp"]) != 0:
        raise SystemError("BLAST+ not found! First install the local BLAST+ tool.")

    if not os.path.exists(db + ".phr"):
        raise FileNotFoundError(f"Database {db} does not exist.")

    if not os.path.exists(query):
        if not query.startswith(">"):
            raise ValueError("Query file must be in Fasta format.")

        tmp = tempfile.mktemp()
        with open(tmp, "w") as f:
            f.write(query)
        query = tmp

    outfmt = defaults.blast_query_settings.pop("outfmt", 5)

    blastp = blast_local.NcbiblastpCommandline(
        query=query,
        db=db,
        outfmt=outfmt,
        out="stdout",
        **defaults.blast_query_settings,
    )
    stdout, stderr = blastp()
    os.remove(tmp)
    return StringIO(stdout)
