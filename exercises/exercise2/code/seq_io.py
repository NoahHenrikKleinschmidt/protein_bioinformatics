"""
This module handles sequence alignments
"""

import defaults

from Bio import SeqIO
from Bio.Align import PairwiseAligner, substitution_matrices
import numpy as np
import os

logger = defaults.logger


aligner = PairwiseAligner()
aligner.mode = "local"
aligner.substitution_matrix = substitution_matrices.load("BLOSUM62")
aligner.mismatch_score = -1


def align_pairwise(seq1, seq2):
    """
    Align two sequences using the default PairwiseAligner

    Parameters
    ----------
    seq1 : str
        The first sequence
    seq2 : str
        The second sequence

    Returns
    -------
    Bio.Align.Alignment
        The alignment object
    """
    alignments = aligner.align(seq1, seq2)
    alignment = next(alignments)
    return alignment


def get_identical_residues(seq1, seq2):
    """
    Get the identical residues through aligning two sequences

    Parameters
    ----------
    seq1 : str
        The first sequence
    seq2 : str
        The second sequence

    Returns
    -------
    tuple
        A tuple of two `numpy.ndarrays` containing the indices of identical residues
        in the two sequences. Indices are relative to unaligned sequences.
        First array is for `seq1`, second array is for `seq2`.
    """

    # first align the two sequences
    alignment = align_pairwise(seq1, seq2)

    # once we have the alignment we need to filter the aligned regions
    # to ensure only identical residues are retained.
    # Note: with proper mismatch penalty this should be fine, and when testing
    # it retained all chunks, but best be on the save side...

    seq1_aligned = alignment.aligned[0]
    seq2_aligned = alignment.aligned[1]
    mask1 = np.zeros(len(seq1_aligned), dtype=bool)
    mask2 = mask1.copy()

    for idx, (s1, s2) in enumerate(zip(seq1_aligned, seq2_aligned)):
        _s1 = seq1[s1[0] : s1[1]]
        _s2 = seq2[s2[0] : s2[1]]
        if _s1 == _s2:
            mask1[idx] = True
            mask2[idx] = True

    seq1_aligned = seq1_aligned[mask1]
    seq2_aligned = seq2_aligned[mask2]

    return seq1_aligned, seq2_aligned


def read_seq_from_pdb(filename):
    """
    Read a sequence from a PDB file

    Parameters
    ----------
    filename : str
        The path to the PDB file

    Returns
    -------
    Bio.SeqRecord.SeqRecord
        The sequence record
    """
    with open(filename, "r") as pdb_file:
        seq_record = SeqIO.read(pdb_file, "pdb-seqres")
    return seq_record


def seq_from_biopandas(df):
    """
    Extract a sequence from a biopandas DataFrame

    Parameters
    ----------
    df : biopandas.pdb.PandasPdb
        The biopandas DataFrame

    Returns
    -------
    Bio.SeqRecord.SeqRecord
        The sequence record
    """
    seq = "".join(df.amino3to1()["residue_name"].values)
    id = "".join(str(os.path.basename(df.pdb_path)).replace(".pdb", ""))
    seq_record = SeqIO.SeqRecord(seq, id=id)
    return seq_record
