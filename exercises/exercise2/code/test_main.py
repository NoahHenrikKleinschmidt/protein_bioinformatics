"""
This script provides the main run for testing purposes
"""

import os

import defaults
from blast_io import BlastQuery
from pdb_io import *
from seq_io import *
from analysis import *
from visualise import *

query = "/Users/noahhk/GIT/protein_bioinformatics/exercises/exercise2/example_data/Q9FJR0.pdb"
query_out = query + ".blast.xml"

if os.path.exists(query_out):
    blast_query = BlastQuery.from_xml(query_out)
else:
    blast_query = BlastQuery.from_pdb(query)
    blast_query.get_ref_sequences()
    blast_query.write(query_out)


ref_sequence = seq_from_biopandas(pdb_to_df(query))

ref_structure = read_pdb(query)
ref_atoms = get_atoms(ref_structure)

structure_queries = StructureQueries.from_ids(blast_query.ref_ids[1:10])
structure_queries.filter()

structure_sequences = structure_queries.to_sequences(full=False)
structures = structure_queries.to_structures()

# superimpose the query structure with the reference structures
variances = superimpose(ref_structure, ref_sequence, structures, structure_sequences)


# # write_variance_table(ref_structure, query + ".var.tsv")
render_structure(ref_structure, query + ".png")
