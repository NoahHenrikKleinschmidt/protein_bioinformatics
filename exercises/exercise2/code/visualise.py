"""
This module handles data visualization
"""

import numpy as np
import defaults
import pdb_io

import os

import pymol.cmd as pymol
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import seaborn as sns


def render_structure(structure, filename, model=0, chain="A"):
    """
    Render a structure using pymol

    Parameters
    ----------
    structure : Bio.PDB.Structure.Structure
        The structure to render.
    filename : str
        The filename to save the structure to.
    model : int, optional
        The index of the model to render, by default 0
    chain : str, optional
        The id of the chain to render, by default "A"
    """
    spinner = defaults.spinner()
    spinner.start("Rendering structure")

    # create a new pymol session
    pymol.reinitialize()

    # load the structure into pymol
    # (turns out the id is just the filename when providing a local file...)
    pymol.load(structure.id)
    pymol.orient()

    pymol.color("gray")

    # color the structure
    set_color = hasattr(next(structure[model][chain].get_residues()), "mean_atom_variance")
    if set_color:

        struct_id = str(os.path.basename(structure.id)).replace(".pdb", "")
        _model = "" if model == 0 else model

        # define a function to get the residue id in a pymol suitable format
        res_id = lambda res: f"/{struct_id}/{_model}/{chain}/{res.resname}`{res.id[1]}/"

        # make a colormap to make octal colors (pymol only accepts octal, WHYYYY)
        # from the structures
        cmap = plt.get_cmap(defaults.cmap)

        for residue in structure[model][chain].get_residues():
            var = residue.mean_atom_variance
            if var != var:
                continue
            color = cmap(var)
            color = rgba_to_octal(color)
            pymol.color(color=color, selection=res_id(residue))

    # render the structure from different angles
    perspectives = (
        Perspective(),
        Perspective(0, 90),
        Perspective(90, 0),
        Perspective(0, 180),
    )
    for p in perspectives:
        p(basename=filename)

    # add a colorbar to the image
    if set_color:
        fig, axs = plt.subplots(2, 2, figsize=(8, 8))

        for (
            p,
            ax,
        ) in zip(perspectives, axs.flat):

            ax.set_title(p.label)
            image = p.filename(basename=filename)
            ax.imshow(plt.imread(image))
            ax.axis("off")

        mapper = cm.ScalarMappable(cmap=cmap, norm=plt.Normalize(vmin=0, vmax=1))
        cbar = fig.colorbar(mapper, shrink=0.25)
        plt.tight_layout()
        fig.savefig(filename, dpi=800, transparent=True, bbox_inches="tight")

    # remove the temporary file
    pymol.delete("all")
    spinner.succeed("Rendering finished")


def variance_hist(variances, filename=None, ax=None, **kwargs):
    """
    Plot a histogram of the variances

    Parameters
    ----------
    variances : numpy.ndarray
        The variances
    filename : str, optional
        A filename to save the plot to.
    ax : matplotlib.axes.Axes, optional
        The axes to plot on, by default None
        If None, a new figure is created
    **kwargs
        Additional keyword arguments to pass to seaborn.histplot
    Returns
    -------
    matplotlib.axes.Axes
        The axes that were used for plotting
    """
    if ax is None:
        fig, ax = plt.subplots()

    sns.histplot(variances, ax=ax, **kwargs)
    ax.set(xlabel="Normalized Residue Variance", ylabel="Residues")
    sns.despine()

    if filename is not None:
        fig.savefig(filename, dpi=500, transparent=True)
    return ax


def rgba_to_octal(rgba):
    """
    Convert a rgba color to an octal color

    Parameters
    ----------
    rgba : tuple
        The rgba color to convert

    Returns
    -------
    str
        The octal color
    """
    return f"0x{int(rgba[0]*255):02x}{int(rgba[1]*255):02x}{int(rgba[2]*255):02x}"


class Perspective:
    """
    Handles a PyMol perspective

    Note
    ----
    Angles are given in degrees not radians.

    Parameters
    ----------
    x : int, optional
        The angle to rotate by in x-direction, by default 0
    y : int, optional
        The angle to rotate by in y-direction, by default 0
    """

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __call__(self, basename=None, filename=None):
        """
        Render the structure with the perspective

        Parameters
        ----------
        basename : str, optional
            The basename of the filename, by default None.
            To this basename the angles and ".png" are appended.
            If None is given then only the angles are used for the filename.
        filename : str, optional
            A full filename to save the rendered image to. This will be taken as is.
        """
        if filename is None:
            filename = self.filename(basename=basename)

        pymol.turn("x", self.x)
        pymol.turn("y", self.y)
        pymol.png(filename, **defaults.pymol_save_kws)

    @property
    def label(self):
        """
        The label of the perspective

        Returns
        -------
        str
            The label
        """
        return f"x:{self.x}° y:{self.y}°"

    def filename(self, basename=None):
        """
        Make a filename for rendering the perspective

        Parameters
        ----------
        basename : str, optional
            The basename of the filename, by default None.
            To this basename the angles and ".png" are appended.
            If None is given then only the angles are used for the filename.

        Returns
        -------
        str
            The filename
        """
        basename = "" if not basename else basename + "."
        return f"{basename}{self.x}x{self.y}y.png"
