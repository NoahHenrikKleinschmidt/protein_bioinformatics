"""
This module handles the main analysis. Namely
- computing the atom-distances in the moving list before and after superimposing
"""

import defaults
import seq_io
import pdb_io

import Bio.PDB as pdb
import numpy as np
import pandas as pd

__default_imposer__ = pdb.Superimposer()


def superimpose(query_structure, query_sequence, ref_structures, ref_sequences, model=0, chain="A"):
    """
    Superimpose a query structure with a list of reference structures

    Note
    ----
    In addition to returning a numpy array of the per-residue mean atom-coordinate variances
    the same data is also amended to the query_structure object directly where each residue is
    annotated with a "mean_atom_variance" attribute.

    Parameters
    ----------
    query_structure : Bio.PDB.Structure.Structure
        The query structure
    query_sequence : Bio.SeqRecord.SeqRecord
        The query sequence (only the parts that have atoms associated in the structure).
    ref_structures : list
        The reference structures
    ref_sequences : list
        The reference sequences (only the parts that have atoms associated in the structures).
    model : int, optional
        The index of the model to superimpose, by default 0.
    chain : str, optional
        The id of the chain to superimpose, by default "A".

    Returns
    -------
    numpy.ndarray
        The variances of of residue-distances within
        the query_structure before and after transformation to each reference structure.
    """

    # get an atoms list for the query_structure
    query_atoms = pdb_io.get_atoms(query_structure)

    spinner = defaults.spinner()
    spinner.start("Superimposing structures...")

    for structure, sequence in zip(ref_structures, ref_sequences):

        # weigh the distances by the alignment score later
        alignment_score = seq_io.align_pairwise(query_sequence.seq, sequence.seq).score

        residues = seq_io.get_identical_residues(query_sequence.seq, sequence.seq)
        atoms = pdb_io.make_atom_lists(structure, query_structure, residues, model=model, chain=chain)
        atom_distances(atoms.fixed, atoms.moving, alignment_score)

    spinner.succeed("Superimposing structures... Done")

    variances = compute_variances(query_structure)
    return variances


def atom_distances(fixed, moving, score):
    """
    Compute the distances between the atoms in the fixed and moving list
    after superimposition.

    Note
    ----
    The distances are divided by the alignment score.
    Additionally, the distances of all atoms are stored in a list attribute "distances"
    in their corresponding residue in the structure object.

    Parameters
    ----------
    fixed : list
        The fixed atoms
    moving : list
        The moving atoms
    score : float
        The alignment score between the two sequences

    Returns
    -------
    numpy.ndarray
        The distances before and after superimposing
    """

    old = [i.get_coord() for i in fixed]
    __default_imposer__.set_atoms(fixed, moving)
    __default_imposer__.apply(moving)
    new = [i.get_coord() for i in moving]

    distances = np.array([np.linalg.norm(i - j) for i, j in zip(new, old)])
    distances /= score

    for atom, dist in zip(moving, distances):
        residue = atom.get_parent()
        if not hasattr(residue, "distances"):
            residue.distances = []
        residue.distances.append(dist)
    return distances


def compute_variances(structure, model=0, chain="A"):
    """
    Compute the variances of the distances

    Note
    ----
    In addition to returning a numpy array of the per-residue mean atom-coordinate variances
    the same data is also amended to the query_structure object directly where each residue is
    annotated with a "mean_atom_variance" attribute. After this function is called
    the residues' "distances" attribute is removed.

    Parameters
    ----------
    structure : Bio.PDB.Structure.Structure
        The structure
    model : int, optional
        The index of the model to superimpose, by default 0.
    chain : str, optional
        The id of the chain to superimpose, by default "A".

    Returns
    -------
    numpy.ndarray
        The variances of atom distances
    """
    structure = list(structure[model][chain].get_residues())
    variances = np.full(len(structure), np.nan)

    for s in structure:
        if not pdb.is_aa(s):
            continue
        if not hasattr(s, "distances"):
            continue
        sdx = structure.index(s)
        variances[sdx] = np.var(s.distances)
        del s.distances

    # normalize the variances
    variances -= np.nanmin(variances)
    variances /= np.nanmax(variances)

    for s, v in zip(structure, variances):
        if not pdb.is_aa(s):
            continue
        s.mean_atom_variance = v

    return variances


def write_variance_table(structure, filename, model=0, chain="A"):
    """
    Write the variance table to a file

    Parameters
    ----------
    structure : Bio.PDB.Structure.Structure
        The structure. This requires that the structure's residues have been
        amended by a `mean_atom_variance` attribute.
    filename : str
        The filename to write to.
    model : int
        The index of the model that was used for superimposing.
    chain : str
        The id of the chain that was used for superimposing.
    """
    residues = list(structure[model][chain].get_residues())
    variances = [i.mean_atom_variance for i in residues]
    pos = [i.id[1] for i in residues]
    names = [i.resname for i in residues]
    model = (model,) * len(pos)
    chain = (chain,) * len(pos)
    df = pd.DataFrame({"model": model, "chain": chain, "pos": pos, "name": names, "variance": variances})
    df.to_csv(filename, index=False, sep="\t")
