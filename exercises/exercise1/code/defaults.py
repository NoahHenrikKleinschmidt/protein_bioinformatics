"""
Default settings
"""

temp_fasta = "temp.fasta"
"""
Temporary fasta file for CLUSTALw
"""

alignment_file = "alignment.aln"
"""
The name of the alignment file produced by CLUSTALw
"""

window_file = "windows.tsv"
"""
The name of the file containing the raw windows
"""

n_windows = 25
"""
The number of windows to use for the sliding window analysis
"""
