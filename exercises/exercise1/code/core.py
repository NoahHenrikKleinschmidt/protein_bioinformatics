"""
This module contains core functions
"""

import logging

logger = logging.getLogger(__name__)


def read_ids(filename):
    """
    Read a file containing a list of UniProt ids (one per line)
    and return a list of ids.

    Parameters
    ----------
    filename : str
        The name of the file containing the ids

    Returns
    -------
    list
        A list of ids
    """
    with open(filename) as f:
        ids = [line.strip() for line in f]
    return ids


def get_sequences(entries):
    """
    Get the sequences for a list of entries

    Parameters
    ----------
    entries : list
        A list of UniProt entries

    Returns
    -------
    list
        A list of sequences
    """
    sequences = []
    for entry in entries:
        typename = type(entry).__name__
        if typename == "SeqRecord":
            sequence = entry.seq
        elif typename == "Record":
            sequence = entry.sequence
        else:
            raise TypeError(f"Unknown entry type {entry.__class__.__name__}")
        sequences.append(sequence)
    return sequences


def write_windows(windows, filename):
    """
    Write the evaluation to a file

    Parameters
    ----------
    windows : list
        A list of evaluations as (start, stop, score) tuples

    filename : str
        The name of the file to write to
    """
    with open(filename, "w") as f:
        f.write("start\tstop\tmean_score\tconserved\n")
        for start, stop, score, conserved in windows:
            f.write(f"{start}\t{stop}\t{score}\t{conserved}\n")


def write_conserved_regions(regions, filename):
    """
    Write the conserved regions to a file

    Parameters
    ----------
    regions : list
        A list of conserved regions as (start, stop) tuples

    filename : str
        The name of the file to write to
    """
    with open(filename, "w") as f:
        f.write("start\tstop\n")
        for start, stop in regions:
            f.write(f"{start}\t{stop}\n")
