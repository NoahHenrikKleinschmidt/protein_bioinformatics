"""
Perform multi-sequence alignment using clustalw
"""

from Bio.Align.Applications import ClustalwCommandline
from Bio.SeqIO import write as write_seqs
from Bio.AlignIO import read as read_align
import os
import defaults
import numpy as np

ALIGNMENT_EVAL = {
    "*": 3,
    ":": 2,
    ".": 1,
    " ": 0,
}
"""
Evaluation key for alignments
"""


def align(seqs, outfile=None):
    """
    Perform multi-sequence alignment using clustalw

    Parameters
    ----------
    seqs : list
        A list of sequences
    outfile : str, optional
        The name of the output file, by default "alignment.aln"

    Returns
    -------
    str
        The alignment in clustal format
    """
    # some default file handling since clustalw wants files
    if not outfile:
        outfile = defaults.alignment_file
        infile = defaults.temp_fasta
    else:
        infile = outfile + ".fasta"
        outfile = outfile + ".aln"

    # write a temp fasta files
    write_seqs(seqs, infile, "fasta")

    # run clustalw
    clustalw_cline = ClustalwCommandline("clustalw2", infile=infile, outfile=outfile)
    _, stdout = clustalw_cline()

    os.remove(infile)
    return stdout


def translate_star(infile):
    """
    Translate the alignment based on the star-annotation line according to the `ALIGNMENT_EVAL` dictionary
    into a numeric scale.

    Parameters
    ----------
    infile : str
        The name of the alignment file

    Returns
    -------
    alignment : Bio.Align.MultipleSeqAlignment
        The alignment with an `eval` attribute containing the per-position numeric translation of the star-annotation
    """
    if not infile:
        infile = defaults.alignment_file
    else:
        infile = infile + ".aln"

    # read alignment output
    alignment = read_align(infile, "clustal")

    # translate star notation
    alignment.eval = [ALIGNMENT_EVAL[i] for i in alignment._star_info]
    return alignment


def evaluate_windows(alignment, window_size=None, threshold=2.0):
    """
    Slide a window of size `window_size` over the alignment and evaluate the window based on the alignment evaluation

    Parameters
    ----------
    alignment : Bio.Align.MultipleSeqAlignment
        The alignment
    window_size : int, optional
        The size of the window to use for the sliding window analysis, by default the window size is computed to yield 25 non-overlapping windows
    threshold : float, optional
        The threshold for the mean window alignment score to call a window 'conserved', by default 2.0

    Returns
    -------
    list
        A list of tuples containing the start and end positions of the window and the mean score of the window, as well as a boolean `True` if a window is considered convserved.
    """
    if not window_size:
        window_size = int(len(alignment[0]) / defaults.n_windows)

    windows = []
    for i in range(len(alignment.eval) - window_size):
        _slice = slice(i, i + window_size)
        window = alignment.eval[_slice]
        _mean = np.mean(window)
        conserved = _mean >= threshold
        windows.append((_slice.start, _slice.stop, _mean, conserved))
    return windows


def get_conserved_regions(windows):
    """
    Get the conserved regions from the alignment and the windows

    Parameters
    ----------
    windows : list
        A list of tuples containing the start and end
        positions of the window, the mean score of the window,
        as well as a boolean `True` if a window is considered convserved.

    Returns
    -------
    list
        A list of tuples containing the start and end positions of the conserved regions
    """
    conserved_regions = []

    # merge adjacent "conserved" windows

    idx = 0
    while idx < len(windows):
        window = windows[idx]
        
        if window[3]:
            start = window[0]

            while idx < len(windows) and window[3]:
                idx += 1
            end = windows[idx - 1][1]

            conserved_regions.append((start, end))
        idx += 1

    return conserved_regions
