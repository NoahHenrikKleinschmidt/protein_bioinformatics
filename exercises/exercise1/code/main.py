"""
Identify regions of high conservation from a multi-sequence protein alignment.
"""

import argparse
import database_io as db
import defaults
import core
import align
import visualise

import logging

logger = logging.getLogger(__name__)


def cli():

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("ids", help="A file containing UniProt or SwissProt ids (one per line) for the query proteins")
    parser.add_argument(
        "-w", "--window-size", type=int, default=None, help=f"The size of the window to use for the sliding window analysis. By default the window size is computed to yield {defaults.n_windows} non-overlapping windows over the aligned sequences"
    )
    parser.add_argument("-t", "--threshold", type=float, default=2.0, help="The threshold for the mean window alignment score to call a window 'conserved'. By default 2.0")
    parser.add_argument("-o", "--output", help="The name of the output file. By default the input filename + 'conserved_regions'.", default=None)
    parser.set_defaults(func=main)
    args = parser.parse_args()
    args.func(args)
    return args


def main(args):

    if not args.output:
        args.output = args.ids

    print("Getting Entries...")
    ids = core.read_ids(args.ids)
    entries = db.get_entries(ids)

    print("Aligning with clustalw2...")
    align.align(entries, outfile=args.output)
    alignment = align.translate_star(args.output)

    print("Sliding windows...")
    windows_file = args.output + ".windows" if args.output else defaults.window_file
    windows = align.evaluate_windows(alignment, args.window_size)
    core.write_windows(windows, windows_file)
    visualise.plot_alignment_scores(windows_file, args.threshold)

    print("Merging conserved regions...")
    regions = align.get_conserved_regions(windows)
    if args.output == args.ids:
        args.output += ".conserved_regions"
    core.write_conserved_regions(regions, args.output)
    print("All done!")


if __name__ == "__main__":
    cli()
