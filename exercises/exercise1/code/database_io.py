"""
This module contains code to get data from Uniprot
"""

from Bio import ExPASy, SwissProt, SeqIO
import urllib
import logging

logger = logging.getLogger(__name__)


def get_entries(ids):
    """
    Get the entries for a list of ids

    Parameters
    ----------
    ids : list
        A list of UniProt ids

    Returns
    -------
    list
        A list of UniProt entries
    """
    entries = []
    for id in ids:
        try:
            entry = get_swissprot_entries([id])
        except:
            entry = get_uniprot_entries([id])

        entries.extend(entry)
    return entries


def get_uniprot_entries(ids: list) -> list:
    """
    Get the UniProt entries for a list of ids

    Parameters
    ----------
    ids : list
        A list of UniProt ids

    Returns
    -------
    list
        A list of UniProt entries
    """
    entries = []
    for id in ids:
        logger.debug(f"Get UniProt entry for {id}")
        url = f"http://www.uniprot.org/uniprot/{id}.xml"
        with urllib.request.urlopen(url) as handle:
            entry = SeqIO.read(handle, "uniprot-xml")
            entries.append(entry)
    return entries



def get_swissprot_entries(ids):
    """
    Get the UniProt entries for a list of ids

    Parameters
    ----------
    ids : list
        A list of UniProt ids

    Returns
    -------
    list
        A list of UniProt entries
    """
    entries = []
    for id in ids:
        logger.debug(f"Get SwissProt entry for {id}")
        with ExPASy.get_sprot_raw(id) as handle:
            entry = SwissProt.read(handle)
            entries.append(entry)
    return entries


if __name__ == "__main__":

    entry = get_uniprot_entries(["Q79FR5"])
    print(entry)
