"""
Visualise the data
"""

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns


def plot_alignment_scores(infile, threshold=2.0, outfile=None):
    """
    Plot the alignment scores

    Parameters
    ----------
    infile : str
        The name of the evaluation file storing the raw windows
    threshold : float, optional
        The threshold for the mean window alignment score to call a window 'conserved', by default 2.0
    outfile : str, optional
        The name of the output file, by default the input filename + "windows.png"
    """

    df = pd.read_csv(infile, sep="\t")

    if not outfile:
        outfile = infile + ".png"

    # plot the data
    sns.set_style("whitegrid")
    sns.set_context("paper")
    sns.set_palette("Set2")
    fig, ax = plt.subplots(figsize=(10, 5))
    sns.lineplot(x="start", y="mean_score", data=df, ax=ax)
    ax.set_xlabel("Position")
    ax.set_ylabel("Mean score")

    # add a threshold line
    ax.axhline(threshold, color="red", linestyle="--")
    ax.text(
        y=threshold + 0.1,
        x=df.start.max(),
        s="threshold",
        color="red",
        ha="right",
    )
    fig.savefig(outfile)
