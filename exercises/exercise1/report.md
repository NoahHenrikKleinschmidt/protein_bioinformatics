---
layout: post
Title: Protein Bioinformatics Exercise Report
---

# Exercise 1 

Nomura Akira, Kleinschmidt Noah

---

For this exercise, we implemented a command-line tool to obtain highly conserved regions from a multi-sequence alignment. 

### Requirements

The script requires 

Default anaconda pre-installed:
- `numpy`
- `pandas`
- `matplotlib`
- `seaborn`

Additional requirements:
- `biopython`
- `argparse` (for the command-line interface)
- `clustalw` 

### Input data

As input data a single datafile containing _UniProt_ or _SwissProt_ ids for query proteins is required, formatted like

```
Q9FJR0
Q9VYS3
Q9EPU0
Q92900
F1RCY6
```
> These are `UPF1` (Rent1) proteins from different species

### Implementation

The algorithm obtains the sequences of the provided proteins from UniProt and uses `clustalw2` to perform multi-sequence alignment. The alignment is then scored from the clustal-output in a sliding-window scheme. Scoring is done by converting the _star-annotation_ of the alignment (`* : . <space>`) into numeric values (`3, 2, 1, 0`). Subsequently, using a threshold, windows are classified as "conserved" or not based on their _mean_ score. Adjacent conserved-windows are then merged into "conserved regions" as final output.

### Output

The script outputs the clustal-output alignment file, as well as two tsv files of sliding-window data, storing the start and stop positions of each window and the final output file containing the start and stop positions of merged windows into conserved regions. Also, a figure showing the mean-score over the sliding windows is produced.

<img src="example_data/ids_upf1.txt.windows.png" alt="Sliding windows">

The above figure shows the output of the sliding windows under default settings. The red line indicates the (default) threshold of `2` required for a window to be classified as "conserved". In this case there are two window-blocks that exceed the threshold line - one starting in the range of positions $\sim 150-350$ and a second one in $\sim 500-900$. The corresponding output file `.conserved_regions` contains exactly these ranges as `start` and `end` indices respectively:

```
start  stop
126    375
485    964
```

More details are available through the `.windows` file which lists all windows and their corresponding mean score:

```
start  stop   mean_score           conserved
0      51     0.9411764705882353   False
1      52     0.9215686274509803   False
2      53     0.9019607843137255   False
...
```

The alignment output from `clustalw` is also saved as `.aln` file to allow the user to subsequently obtain the respective aligned sequences from conserved regions of their proteins of interest. 

### Usage

The code is available [via GitLab](https://gitlab.com/NoahHenrikKleinschmidt/protein_bioinformatics), located in the `exercises/exercise1/code` directory.

    >>> git clone https://gitlab.com/NoahHenrikKleinschmidt/protein_bioinformatics

To use the script call (from within `exercises/exercise1/code`)

    >>> python3 main.py <ids-file>

The full CLI reads as follows:

```
usage: main.py [-h] [-w WINDOW_SIZE] [-t THRESHOLD] [-o OUTPUT] ids

Identify regions of high conservation from a multi-sequence protein alignment.

positional arguments:
  ids                   A file containing UniProt or SwissProt ids (one per line) 
                        for the query proteins

optional arguments:
  -h, --help            show this help message and exit
  -w WINDOW_SIZE, --window-size WINDOW_SIZE
                        The size of the window to use for the sliding window analysis. 
                        By default the window size
                        is computed to yield 25 windows over the sequence
  -t THRESHOLD, --threshold THRESHOLD
                        The threshold for the mean window alignment score to call 
                        a window 'conserved'
  -o OUTPUT, --output OUTPUT
                        The name of the output file
```
