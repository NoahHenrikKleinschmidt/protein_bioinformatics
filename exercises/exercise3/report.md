---
layout: post
Title: Protein Bioinformatics Exercise Report
---

# Exercise 3

Nomura Akira, Kleinschmidt Noah

---

For this exercise, we implemented a command-line tool and accompanying Snakemake pipeline, which employs `openMM` to perform molecular dynamic simulations starting with PDB input files to study the dynamics of short peptides in aqueous solution. 

### Requirements

The script requires 

Default anaconda pre-installed:
- `numpy`
- `pandas`

Additional requirements:
- `openmm`
- `mdtraj`
- `nglview`
- `snakemake` (for automatic notebook generation)


> There is a dedicated `openmm.yaml` environments specification file which will set up a suitable environment.

### Input data

For the input, the script requires a valid `PDB` file. 

### Implementation

The primary implementation consists of a wrapper class to make `openMM` simulations easier. The main script uses the implemented class alongside some further input options to generate trajectory files for the given PDB file. Additionally, a tiny `snakemake` pipeline takes the output of the main script to automatically assemble a `jupyter` notebook to visualize the trajectories using _nglview_. 


### Output

The script outputs two trajectory files (one from energy minimization and one from dynamic simulations) alongside corresponding tabular files for physical data of the given system along the simulation steps. 

### Usage

The code is available [via GitLab](https://gitlab.com/NoahHenrikKleinschmidt/protein_bioinformatics), located in the `exercises/exercise3/code` directory.

    >>> git clone https://gitlab.com/NoahHenrikKleinschmidt/protein_bioinformatics

To use the script call (from within `exercises/exercise3/code`)

    >>> python3 main.py <pdb-file> -f <at least one forcefield> -t <temperature> -s <time step>

The full CLI reads as follows:

```
usage: main.py [-h] [-f FORCEFIELD [FORCEFIELD ...]] [-t TEMPERATURE] [-s TIMESTEP] [--solvate]
               [-o OUTPUT] [-p {CPU,CUDA}] [--nsteps NSTEPS] [--sample_steps SAMPLE_STEPS]
               pdb_file

This script runs a molecular dynamics simulation using openMM of a protein in water.

positional arguments:
  pdb_file              The path to the PDB file

optional arguments:
  -h, --help            show this help message and exit
  -f FORCEFIELD [FORCEFIELD ...], --forcefield FORCEFIELD [FORCEFIELD ...]
                        The name(s) of the forcefield(s) to use
  -t TEMPERATURE, --temperature TEMPERATURE
                        The temperature of the simulation in kelvin.
  -s TIMESTEP, --timestep TIMESTEP
                        The timestep of the simulation in femtoseconds.
  --solvate             Solvate the system - this will add water as solvent.
  -o OUTPUT, --output OUTPUT
                        The name of the output directory (default: same as PDB input directory)
  -p {CPU,CUDA}, --platform {CPU,CUDA}
                        The name of the platform to use (default: CPU)
  --nsteps NSTEPS       The number of steps to run the simulation for (default: 1e6)
  --sample_steps SAMPLE_STEPS
                        The number of steps between samples (default: 1000)
```

#### Snakemake

The `snakemake` pipeline is primarily designed to assemble a jupyter notebook automatically__ to view the trajectory in. It can, also call on the main script, however. To control the parameters of the CLI a `config` file can be edited to that end. The pipeline can be called just like any other snakemake pipeline e.g.

    $ snakemake my_pdb.ipynb -j 1 

> To make a notebook output from an inputfile named `my_pdb.pdb` this will automatically call the main script if necessary. 
>


### Results

We analysed the provided four PDB files of small peptides with substituted amino acids _Asp_, _Phe_, _Tyr_, and _Lys_, over $10^7$ steps of $2\text{ fs}$ each. The resulting dynamics suggested that all peptides tended to fold into similarly unstable pseudo-helical structures - none of them folded into a true stable $\alpha$-helix though. 


<img src="resources/structs_figure.png">

> Views on peptide foldings. A cartoon representation of the peptide-backbone is shown in rainbow colors from N- to C-terminus. Additionally, amino acids are shown as ball-and-stick representations. (A) Tyr-Peptide, (B) Asp-Peptide, (C) Phe-Peptide, (D) Lys-Peptide.
>

As for possible factors relating to catalytic turnover of Acetophenone, aside from steric hindrance (explaining the low turnover rate of Lys), the proximal presence of a conjugated $\pi$-system as provided by amino acids such as _Phe_  or _Tyr_  might play a role in the elevated catalytic rate of these peptides compared to their non-conjugated counterparts _Lys_ and _Asp_. 

<img src="resources/structs_figure2.png">

> View of _His_ (orange) and proximal amino acids (cyan) in the (A) Tyr-Peptide, (B) Asp-Peptide, (C) Phe-Peptide, and (D) Lys-Peptide. The views show the peptide state with closest proximity of _His_ to the respective proximal side-chain. 
