"""
This module handles setting up a simulation using MDAnalysis and OpenMM
"""

import openmm.app as app
import openmm as mm
import openmm.unit as units
import MDAnalysis as md


class Simulation:
    """
    This class sets up a simulation using MDAnalysis and OpenMM
    """

    _default_create_system_kws = dict(
        nonbondedMethod=app.PME,
        nonbondedCutoff=1.0 * units.nanometer,
        constraints=app.HBonds,
        rigidWater=True,
        ewaldErrorTolerance=0.0005,
    )
    _default_minimize_kws = dict(tolerance=5 * units.kilojoules_per_mole / units.nanometer, maxIterations=1000, data=dict(step=True, separator="\t", potentialEnergy=True, temperature=True))

    _default_store_data = dict(step=True, time=True, progress=True, temperature=True, volume=True, density=True, potentialEnergy=True, kineticEnergy=True, totalEnergy=True, separator="\t")

    def __init__(self, pdb_file, forcefield, temperature, timestep, platform):
        """
        This function initializes the simulation object

        Parameters
        ----------
        pdb_file : str
            The path to the PDB file
        forcefield : str or list
            The name(s) of the forcefield(s) to use
        temperature : float
            The temperature of the simulation in kelvin.
        timestep : float
            The timestep of the simulation in femtoseconds.
        platform : str
            The name of the platform to use
        """

        self.pdb_file = pdb_file
        if isinstance(forcefield, str):
            forcefield = [forcefield]
        self.forcefield = app.ForceField(*forcefield)
        self.temperature = temperature
        self.timestep = timestep
        self._filename = pdb_file.replace(".pdb", "")

        # self.universe = md.Universe(pdb_file)
        self._pdb_src = app.PDBFile(pdb_file)

        # Set the platform
        self.platform = mm.Platform.getPlatformByName(platform)

        # Create the integrator
        self.integrator = mm.LangevinIntegrator(
            self.temperature * units.kelvin,
            1.0 / units.picosecond,
            self.timestep * units.femtoseconds,
        )

        self.system = None
        # self.context = None

    def setup(self, **kwargs):
        """
        Sets up the simulation

        Parameters
        ----------
        kwargs : dict
            The keyword arguments to pass to forcefield.createSystem.
        """

        kwargs = {**self._default_create_system_kws, **kwargs}

        # Create the OpenMM system
        self.system = self.forcefield.createSystem(self._pdb_src.topology, **kwargs)

        # # Create the simulation context
        # self.context = mm.Context(self.system, self.integrator)

        # # Set the positions
        # self.context.setPositions(self._pdb_src.positions)

    def solvate(self, model="tip3p", pad=1.0):
        """
        This function solvates the system

        Parameters
        ----------
        model : str
            The name of the solvent model to use
        pad : float
            The padding to use in nanometers
        """

        # Create the modeller
        modeller = app.Modeller(self._pdb_src.topology, self.system)

        # Add solvent
        modeller.addHydrogens(self.forcefield)
        modeller.addSolvent(self.forcefield, model=model, padding=pad * mm.nanometers)

        # Update the positions
        self._pdb_src.positions = modeller.getPositions()

    def run(self, steps, outfile=None, sample_steps=None, minimize_kws=None, data=None):
        """
        This function runs the simulation

        Parameters
        ----------
        steps : int
            The number of steps to run
        outfile : str
            The path to the output file.
            By default the input PDB-filename is used.
            Note, this is interpreted as a basename
            and the following files will be created:

                - outfile.min.dcd
                - outfile.min.data
                - outfile.md.dcd
                - outfile.md.data

        sample_steps : int
            The number of steps after which to sample. By default this is adjusted
            to get 1000 samples.

        minimize_kws : dict
            The keyword arguments to pass to simulation.minimizeEnergy. This may include a dictionary
            entry with key `data` which specifies the data to be stored into the `outfile.min.data` file.
            The entries of this dictionary are passed to `StateDataReporter`). Additionally this may contain an
            entry `record_step` specifying at which intervals records are stored into `outfile.min.dcd` and `outfile.min.data`.
            If this is not provided, an interval is automatically determined.

        data : dict
            The data to be stored into the `outfile.md.data` file. The entries of this dictionary are
            passed to `StateDataReporter`).
        """

        # Set the output file
        if not outfile:
            outfile = self._filename

        if not sample_steps:
            sample_steps = int(steps / 1000)

        # Create the simulation
        simulation = app.Simulation(self._pdb_src.topology, self.system, self.integrator, self.platform)

        # Set the platform
        simulation.context.setPositions(self._pdb_src.positions)

        # Minimize the energy
        minimize_kws = {} if not minimize_kws else minimize_kws
        minimize_kws = {**self._default_minimize_kws, **minimize_kws}
        _min_data = minimize_kws.pop("data")
        min_step = minimize_kws.pop("record_step", None)
        simulation.minimizeEnergy(**minimize_kws)

        minimize_file = outfile + ".min.dcd"
        minimize_data = outfile + ".min.data"

        if not min_step:
            i = minimize_kws["maxIterations"]
            if i <= 20:
                min_step = 1
            elif i <= 500:
                min_step = 10
            elif i <= 2000:
                min_step = 100
            elif i <= 20000:
                min_step = 1000
            else:
                min_step = 10000
        simulation.reporters.append(app.DCDReporter(minimize_file, reportInterval=min_step))
        simulation.reporters.append(app.StateDataReporter(minimize_data, reportInterval=min_step, **_min_data))

        # Run the simulation
        md_file = outfile + ".md.dcd"
        md_data = outfile + ".md.data"
        data = {} if not data else data
        data = {**self._default_store_data, **data}
        simulation.reporters.append(app.DCDReporter(md_file, reportInterval=sample_steps))
        simulation.reporters.append(app.StateDataReporter(md_data, reportInterval=sample_steps, totalSteps=steps, **data))

        simulation.step(steps)

        self._filename = outfile

