"""
This script runs a molecular dynamics simulation using openMM of a protein in water. 
"""

import argparse
import os


def cli():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("pdb_file", help="The path to the PDB file")
    parser.add_argument(
        "-f",
        "--forcefield",
        help="The name(s) of the forcefield(s) to use",
        nargs="+",
    )
    parser.add_argument("-t", "--temperature", help="The temperature of the simulation in kelvin.", type=float)
    parser.add_argument("-s", "--timestep", help="The timestep of the simulation in femtoseconds.", type=float)
    parser.add_argument("--solvate", help="Solvate the system - this will add water as solvent.", action="store_true")
    parser.add_argument("-o", "--output", help="The name of the output directory (default: same as PDB input directory)", default=None)
    parser.add_argument("-p", "--platform", help="The name of the platform to use (default: CPU)", choices=["CPU", "CUDA"], default="CPU")
    parser.add_argument(
        "--nsteps",
        help="The number of steps to run the simulation for (default: 1e6)",
        type=int,
        default=1000000,
    )
    parser.add_argument(
        "--sample_steps",
        help="The number of steps between samples (default: 1000)",
        type=int,
        default=1000,
    )
    return parser.parse_args()


def main():
    args = cli()
    from simulation import Simulation

    sim = Simulation(
        args.pdb_file,
        args.forcefield,
        args.temperature,
        args.timestep,
        args.platform,
    )
    if args.solvate:
        sim.solvate()
    sim.setup()

    if args.output is None:
        args.output = args.pdb_file.replace(".pdb", "")
    else:
        os.makedirs(args.output, exist_ok=True)
        args.output = os.path.join(args.output, os.path.basename(args.pdb_file).replace(".pdb", ""))

    sim.run(args.nsteps, outfile=args.output, sample_steps=args.sample_steps)


if __name__ == "__main__":
    main()
