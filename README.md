# The repository for the  protein bioinformatics course

This repository contains the code for the exercises in the protein bioinformatics course. 

## Exercises 1 

We wrote a command line tool that accepts a file of protein identifiers (SwissProt or UniProt), gets the protein sequences, performs multiple-sequence alignment (MSA) using `clustalw`, and finally identifies regions of high conservation. Regions of high conservation are identified through a sliding window approach with a threshold. 

<img src="exercises/exercise1/resources/overview.png">

## Exercises 2 

We wrote a command line tool that starts with a query structure (a PDB file) and compute the residues' positional variation through pairwise superimposition of reference structures from related proteins.

<img src="exercises/exercise2/resources/overview.png">

---


<img src="exercises/exercise2/results/Q9FJR0.var.png">

## Exercises 3

We wrote a command-line-tool and accompanying snakemake pipeline to perform molecular dynamics simulation from a PDB input file using `openMM`. The final output are molecular trajectory files and physical-data of the simulated system, alongside (through the pipeline) a jupyter notebook visualising the trajectory using _nglview_. At the core of the implementation is a wrapper class `Simulation` that allows a quicker setup of basic molecular dynamic simulations using openMM by automatically setting up necessary auxiliaries such as the Integrator. 

<img src="exercises/exercise3/resources/structs_figure.png">
